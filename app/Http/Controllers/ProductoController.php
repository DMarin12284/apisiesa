<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = producto::all();

        return response()->json($productos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        try{
            //Instanciamos la clase Productos
            $producto = new producto;
            //Declaramos el nombre con el nombre enviado en el request
            $producto->titulo = $data['titulo'];
            $producto->descripcion = $data['descripcion'];
            $producto->precio = $data['precio'];
            $producto->descuento = $data['descuento'];
            $producto->img = $data['imagen'];
            //Guardamos el cambio en nuestro modelo
            $producto->save();
            return \Response::json(array(
                'error' => false,
                'data' => $producto->toArray()),
                200
            );
        }
        catch (\Exception $e) {
            return \Response::json(array(
                'error' => true,
                'data' => array()),
                500
            );
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Solicitamos al modelo el Producto con el id solicitado por GET.
        return Producto::where('id', $id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = json_decode($request->getContent(), true);
        try{
            $producto = producto::find($id);

            if($data['titulo']){
            $producto->titulo = $data['titulo'];
            }
            if($data['descripcion']){
            $producto->descripcion = $data['descripcion'];
            }
            if($data['precio']){
            $producto->precio = $data['precio'];
            }
            if($data['descuento']){
            $producto->descuento = $data['descuento'];
            }
            if($data['imagen']){
            $producto->imagen = $data['imagen'];
            }
            //Guardamos el cambio en nuestro modelo
            $producto->save();
            return \Response::json(array(
                'error' => false,
                'data' => $producto->toArray()),
                200
            );
        }
        catch (\Exception $e) {
            return \Response::json(array(
                'error' => true,
                'data' => array()),
                500
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
